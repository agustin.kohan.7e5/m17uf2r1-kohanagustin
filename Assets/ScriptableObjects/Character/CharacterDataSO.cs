
using UnityEngine;

public class CharacterDataSO : ScriptableObject
{
    public string Name;
    public float _maxHealth;

    [SerializeField]
    private float _currentHealth;
    public float CurrentHealth
    {
        get { return _currentHealth; }
        set
        {
            _currentHealth = Mathf.Clamp(value, 0, _maxHealth);
        }
    }
    public float _initialSpeed;
    public float CurrentSpeed;
    public void ResetSpeed() => CurrentSpeed = _initialSpeed;

    public float _initialForceMelee;
    public float CurrentForceMelee;
}
