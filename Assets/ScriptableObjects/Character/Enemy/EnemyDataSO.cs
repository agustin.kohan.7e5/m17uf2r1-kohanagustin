using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "ScriptableObjects/EnemyData", order = 0)]
public class EnemyDataSO : CharacterDataSO
{
    public int Score;
    public int Coins;

    public void SetValues(EnemyDataSO enemyDataToClone)
    {
        Name = enemyDataToClone?.Name;
        _maxHealth = enemyDataToClone._maxHealth;
        CurrentHealth = enemyDataToClone.CurrentHealth;
        _initialSpeed = enemyDataToClone._initialSpeed;
        CurrentSpeed = enemyDataToClone._initialSpeed;
        _initialForceMelee = enemyDataToClone._initialForceMelee;
        CurrentForceMelee = enemyDataToClone._initialForceMelee;
        Score = enemyDataToClone.Score;
        Coins = enemyDataToClone.Coins;
    }

    public void resetHealth() => CurrentHealth = _maxHealth;
}
