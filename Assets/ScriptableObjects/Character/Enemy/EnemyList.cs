using UnityEngine;

[CreateAssetMenu(fileName = "EnemyList", menuName = "ScriptableObjects/EnemyList", order = 5)]
public class EnemyList : ScriptableObject
{
    [SerializeField]
    private GameObject[] _enemyArray;
    public GameObject[] EnemyArray { get { return _enemyArray; } }
}
