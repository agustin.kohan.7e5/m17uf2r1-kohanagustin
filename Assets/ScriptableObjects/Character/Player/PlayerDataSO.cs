using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "ScriptableObjects/PlayerData", order = 0)]
public class PlayerDataSO : CharacterDataSO
{
    public float DashForce;
    public float DashCooldown;
    public float DashDuration;
    public int CurrentCoins;
    public float CurrentScore;
    public Inventory inventoryToClone;
    public Inventory inventory;

    public void ResetParameters()
    {
        CurrentHealth = _maxHealth;
        CurrentScore = 0;
        CurrentCoins = 0;
        CurrentSpeed = _initialSpeed;
    }

    public void SetValues(PlayerDataSO playerDataToClone)
    {
        Name = playerDataToClone?.Name;
        _maxHealth = playerDataToClone._maxHealth;
        CurrentHealth = playerDataToClone.CurrentHealth;
        _initialSpeed = playerDataToClone._initialSpeed;
        CurrentSpeed = playerDataToClone.CurrentSpeed;
        _initialForceMelee = playerDataToClone._initialForceMelee;
        CurrentForceMelee = playerDataToClone.CurrentForceMelee;
        DashForce = playerDataToClone.DashForce;
        DashCooldown = playerDataToClone.DashCooldown;
        DashDuration = playerDataToClone.DashDuration;
        CurrentCoins = playerDataToClone.CurrentCoins;
        CurrentScore = playerDataToClone.CurrentScore;
        inventory = (Inventory)ScriptableObject.CreateInstance(typeof(Inventory));
        inventory.SetValues(playerDataToClone.inventoryToClone);
    }
}