using System;
using UnityEngine;

[CreateAssetMenu(fileName = "InventoryData", menuName = "ScriptableObjects/InventoryData", order = 6)]
public class Inventory : ScriptableObject
{
    public int WeaponArrayIndex;
    public WeaponDataSO[] WeaponDataArray = new WeaponDataSO[5];
    public ItemDataSO[] ConsumibleItemArray = new ItemDataSO[2];

    public void SetValues(Inventory inventoryToClone)
    {
        WeaponDataArray[0] = inventoryToClone.WeaponDataArray[0];
    }
    public void ClearInventory()
    {
        WeaponArrayIndex = 0;
        for (int i = 1; i < WeaponDataArray.Length; i++)
            WeaponDataArray[i] = null;

        for (int i = 0; i < ConsumibleItemArray.Length; i++)
            ConsumibleItemArray[i] = null;
    }
}
