public class ConsumibleSO : ItemDataSO
{
    public virtual void Consume(int InventorySlot) 
    { 
        GameManager.Instance.Player.GetComponent<Player>()._playerData.inventory.ConsumibleItemArray[InventorySlot] = null; 
    }
}
