using UnityEngine;

[CreateAssetMenu(fileName = "DemonCurseSO", menuName = "ScriptableObjects/Consumibles/DemonCurse", order = 0)]
public class DemonCurseSO : ConsumibleSO
{
    public float MaxHealthDecreased;
    public float SpeedPorcentageBoost;

    public override void Consume(int InventorySlot)
    {
        var player = GameManager.Instance.Player.GetComponent<Player>();
        player._playerData._maxHealth -= MaxHealthDecreased;
        if (player._playerData._maxHealth < player._playerData.CurrentHealth)
        {
            player._playerData.CurrentHealth = player._playerData._maxHealth;
            if (player._playerData.CurrentHealth <= 0) player.Die();
            
        }

        player._playerData.CurrentSpeed += (player._playerData.CurrentSpeed * (SpeedPorcentageBoost / 100));
        base.Consume(InventorySlot);
    }
}
