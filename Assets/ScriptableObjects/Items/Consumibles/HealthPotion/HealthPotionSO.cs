using UnityEngine;

[CreateAssetMenu(fileName = "HealthPotionSO", menuName = "ScriptableObjects/Consumibles/HealthPotion", order = 0)]
public class HealthPotionSO : ConsumibleSO
{
    public float HealthRecovered;
     public override void Consume(int InventorySlot)
     {
        GameManager.Instance.Player.ReciveHeal(HealthRecovered);
        base.Consume(InventorySlot);
     }
}
