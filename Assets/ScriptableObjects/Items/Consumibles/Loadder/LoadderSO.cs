using UnityEngine;
[CreateAssetMenu(fileName = "LoadderSO", menuName = "ScriptableObjects/Consumibles/Loadder", order = 2)]
public class LoadderSO : ConsumibleSO
{
    public int LoadderCharged;

    public override void Consume(int InventorySlot)
    {
        var player = GameManager.Instance.Player.GetComponent<Player>();
        player._playerData.inventory.WeaponDataArray[player._playerData.inventory.WeaponArrayIndex].CurrentLoaders += LoadderCharged;
        UIManager.Instance.UpdateWeaponInventory();
        base.Consume(InventorySlot);
    }
}
