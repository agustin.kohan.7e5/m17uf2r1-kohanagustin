using UnityEngine;

[CreateAssetMenu(fileName = "ItemDataSO", menuName = "ScriptableObjects/ItemDataSO", order = 7)]
public class ItemDataSO : ScriptableObject
{
    public string Name;
    public string Description;
    public Sprite ItemSprite;
    public int Price;
    //public int chance;
}