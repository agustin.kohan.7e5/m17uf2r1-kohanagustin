using UnityEngine;

[CreateAssetMenu(fileName = "ItemList", menuName = "ScriptableObjects/ItemList", order = 6)]
public class ItemListSO : ScriptableObject
{
    public GameObject[] itemArray;
}
