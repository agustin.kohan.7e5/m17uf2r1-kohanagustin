using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponDataSO", menuName = "ScriptableObjects/WeaponDataSO", order = 3)]
public class WeaponDataSO : ItemDataSO
{
    public GameObject Bullet;
    public float MaxAmmo;
    public float CurrentAmmo;
    public int CurrentLoaders;
    public float Cadence;
    public float ReloadTime;

    [SerializeField]
    private float _dispersion;
    public float Dispersion { get { return _dispersion / 100; } }

    public float BulletXShot;
    public int Damage;

    public void LoadValues(WeaponDataSO weaponDataSO)
    {
        weaponDataSO = this;
    }

    public class Attack { };
}
