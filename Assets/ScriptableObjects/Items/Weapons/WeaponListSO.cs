using UnityEngine;

[CreateAssetMenu(fileName = "WeaponList", menuName = "ScriptableObjects/WeaponList", order = 0)]
public class WeaponListSO : ScriptableObject
{
    public WeaponDataSO Pistol;
    public WeaponDataSO Shotgun;
    public WeaponDataSO AssaultRifle;
    public WeaponDataSO Sniper;
    public WeaponDataSO BouncyGun;
}
