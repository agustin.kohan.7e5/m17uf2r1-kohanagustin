using System.Collections;
using UnityEngine;

public enum moveState
{
    Active,
    Stuned,
    Still
}

public abstract class Character : MonoBehaviour
{
    public CharacterDataSO _characterData;
    protected Rigidbody2D _rb;
    public moveState _moveState = moveState.Active;
    protected BoxCollider2D _collider;

    protected virtual void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _collider = GetComponent<BoxCollider2D>();
    }

    public void Stun(float StunTime) => StartCoroutine(StayStunedDuringTime(StunTime));

    private IEnumerator StayStunedDuringTime(float StunTime)
    {
        _moveState = moveState.Stuned;
        _rb.velocity = Vector2.zero;
        yield return new WaitForSeconds(StunTime); 
        _moveState = moveState.Active;
    }

    public abstract void Die();
}
