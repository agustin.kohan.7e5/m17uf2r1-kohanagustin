using UnityEngine;

public class Enemy : Character, IDamageable
{
    [SerializeField]
    protected EnemyDataSO _enemyData;

    private AudioSource audioSource;

    public void TakeDamage(float damageTaken)
    {
        _enemyData.CurrentHealth -= damageTaken;
        if (_enemyData.CurrentHealth <= 0)
            Die();
    }

    protected override void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        SetSOValues();
        base.Awake();
    }

    public override void Die()
    {        
        audioSource.Play();
        GameManager.Instance.AddCoinsToPlayer((_characterData as EnemyDataSO).Coins);
        GameManager.Instance.AddScoreToPlayer((_characterData as EnemyDataSO).Score);
        GameManager.Instance.CurrentAmountOfEnemyInScene--;
        UIManager.Instance.UpdateCurrentCoins();
        UIManager.Instance.UpdateScoreText();
        if (GameManager.Instance.ShoopCounter < 5)
        {
            GameManager.Instance.ShoopCounter++;
            UIManager.Instance.UpdateShoopBar();
        }
        Destroy(this.gameObject);
    }

    void SetSOValues()
    {
        _enemyData = (EnemyDataSO)ScriptableObject.CreateInstance(typeof(EnemyDataSO));
        _enemyData.SetValues(_characterData as EnemyDataSO);
        _enemyData.CurrentHealth = _enemyData._maxHealth;
        _characterData = _enemyData;
    }
}
