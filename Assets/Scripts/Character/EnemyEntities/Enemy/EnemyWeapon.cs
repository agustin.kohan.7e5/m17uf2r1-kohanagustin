using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    [SerializeField]
    WeaponDataSO _weaponData;
    float _currentAmmo;
    WeaponState _weaponState;
    GameObject _bullet;



    protected void Awake()
    {
        _currentAmmo = _weaponData.MaxAmmo;
        _weaponState = WeaponState.Active;

        
    }


    private void Update()
    {
        Shoot();
        Reload();
    }

    public virtual void Shoot()
    {
        if (_weaponState == WeaponState.Active && _currentAmmo > 0)
        {
            _bullet = Instantiate(_weaponData.Bullet, transform.position, new Quaternion(transform.parent.rotation.x, transform.parent.rotation.y,
            transform.parent.rotation.z + Random.Range(-_weaponData.Dispersion, _weaponData.Dispersion), transform.parent.rotation.w));

            _bullet.GetComponent<Bullet>().Damage = _weaponData.Damage;
            _currentAmmo -= _weaponData.BulletXShot;
            StartCoroutine(Shooting(_weaponData.Cadence));

        }
    }

    protected IEnumerator Shooting(float cadenceShoot)
    {
        _weaponState = WeaponState.Shooting;
        yield return new WaitForSecondsRealtime(cadenceShoot);
        _weaponState = WeaponState.Active;
    }

    public virtual void Reload()
    {
        if (_weaponState == WeaponState.Active && _currentAmmo < _weaponData.MaxAmmo)
        {
            _weaponState = WeaponState.Reloading;
            StartCoroutine(Reloading(_weaponData.ReloadTime));
        }

    }

    IEnumerator Reloading(float reloadTime)
    {
        _weaponState = WeaponState.Reloading;
        yield return new WaitForSecondsRealtime(reloadTime);
        _currentAmmo = _weaponData.MaxAmmo;
        _weaponState = WeaponState.Active;

    }
}
