using System.Collections;
using UnityEngine;

public class KamikazeAttack : Enemy
{


    Vector3 _playerPosition;
    [SerializeField]
    float _distanceToExplode;
    [SerializeField]
    float _explosionForce;
    [SerializeField]
    float _spareTime;

    private void Update()
    {
        _playerPosition = GameManager.Instance.GetPlayerPosition();
        if (Vector2.Distance(transform.position, _playerPosition) <= _distanceToExplode && _moveState == moveState.Active)
            StartCoroutine(WaitUntilExplode());
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (_moveState == moveState.Active)
            ChasePlayer();
    }

    private void ChasePlayer()
    {
        float z = transform.eulerAngles.z + 90;
        _rb.velocity = _enemyData._initialSpeed * new Vector2(Mathf.Cos(z * Mathf.Deg2Rad), Mathf.Sin(z * Mathf.Deg2Rad));
    }

    private void Explode()
    {
        _collider.enabled = false;
        foreach (RaycastHit2D hit in Physics2D.CircleCastAll(transform.position, _distanceToExplode, Vector2.zero, _collider.size.x + 0.5f))
        {
            if (hit.collider.TryGetComponent<IDamageable>(out IDamageable damageable))
                damageable.TakeDamage(_enemyData.CurrentForceMelee);

            if (hit.collider.TryGetComponent<IKnockeable>(out IKnockeable knockeable))
                knockeable.ReciveKnockBack(hit.normal, _explosionForce);   
        }
    }

    IEnumerator WaitUntilExplode()
    {
        _moveState = moveState.Still;
        _rb.velocity = Vector2.zero;
        yield return new WaitForSecondsRealtime(_spareTime);
        
        Explode();
        Die();
    }
}
