using UnityEngine;

public class PointToPlayer : MonoBehaviour
{
    Vector3 _playerPosition;

    private void Update()
    {
        _playerPosition = GameManager.Instance.GetPlayerPosition();
    }

    void FixedUpdate()
    {
        LookAtPlayer();
    }

    void LookAtPlayer()
    {
        Vector2 direction = _playerPosition - transform.position;
        float _angle = Vector2.SignedAngle(Vector2.right, direction);
        transform.eulerAngles = new Vector3(0, 0, _angle - 90);
    }
}
