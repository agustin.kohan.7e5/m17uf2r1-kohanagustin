using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public int NumberOfEnemies;
    [SerializeField]
    EnemyList _enemyList;
    private RoomsEvent _roomsEvent;

    BoxCollider2D _boxCollider;

    // Start is called before the first frame update
    void Awake()
    {
        _boxCollider = transform.parent.GetComponent<BoxCollider2D>();
        _roomsEvent = transform.parent.transform.Find("EnterDetector")?.GetComponent<RoomsEvent>();
        _roomsEvent.OnEnter += SpawnEnemies;
    }

    private void SpawnEnemies(object sender, System.EventArgs e)
    {
        SpawnNewRoundOfEnemies();
        _roomsEvent.OnEnter -= SpawnEnemies;
    }

    public void SpawnNewRoundOfEnemies()
    {
        for (int i = 0; i < Random.Range(1, NumberOfEnemies); i++)
        {
            SetRandomPosition();
            SpawnNewEnemy(ChoseRandomEnemy());
        }
    }

    GameObject ChoseRandomEnemy()
    {
        return _enemyList.EnemyArray[Random.Range(0,_enemyList.EnemyArray.Length)];
    }

    private void SetRandomPosition()
    {
        transform.position = new Vector2(Random.Range(_boxCollider.bounds.min.x + 1, _boxCollider.bounds.max.x - 1), Random.Range(_boxCollider.bounds.min.y + 1, _boxCollider.bounds.max.y - 1));
    }

    void SpawnNewEnemy(GameObject enemy)
    {
        Instantiate(enemy, transform.position, transform.rotation);
        GameManager.Instance.CurrentAmountOfEnemyInScene++;
    }
}
