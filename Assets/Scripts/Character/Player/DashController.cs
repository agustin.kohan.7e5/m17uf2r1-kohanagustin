using System.Collections;
using UnityEngine;

public class DashController : MonoBehaviour
{
    enum DashState
    {
        Active,
        Disabled
    }

    DashState _dashState;
    Player player;
    Rigidbody2D _rb;
    
    // Start is called before the first frame update
    void Awake()
    {
        player = GetComponent<Player>();
        _rb = GetComponent<Rigidbody2D>();
        _dashState = DashState.Active;
    }

    public bool Dash(Vector2 dashDirection)
    {
        if (Input.GetButtonDown("Dash") && _dashState == DashState.Active)
        {
            StartCoroutine(WaitDuringDash());
            _rb.AddForce(1000 * player._playerData.DashForce * dashDirection);
            return true;
        }
        return false;
    }

    IEnumerator WaitDuringDash()
    {
        _dashState = DashState.Disabled;
        yield return new WaitForSeconds(player._playerData.DashCooldown);
        _dashState = DashState.Active;
    }
}
