using UnityEngine;

public class InventoryController : MonoBehaviour
{
    Inventory _inventory;
    Weapon _weapon;
    private void Start()
    {
        _inventory = gameObject.GetComponent<Player>()._playerData.inventory;
        _weapon = gameObject.GetComponentInChildren<Weapon>();

        _weapon._weaponData = _inventory.WeaponDataArray[0];
    }

    void Update()
    {
        if (Input.GetButtonDown("ScrollUpInList") || Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (_inventory.WeaponArrayIndex < _inventory.WeaponDataArray.Length && _inventory.WeaponDataArray[_inventory.WeaponArrayIndex+1] != null)
                ChangeWeapon(1);
        }
        else if (Input.GetButtonDown("ScrollDownInList") || Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (_inventory.WeaponArrayIndex > 0)
                ChangeWeapon(-1);
        }
        UseConsumibleObject();
    }

    void ChangeWeapon(int i)
    {
        _inventory.WeaponArrayIndex += i;
        _weapon.ChangeWeaponValues(_inventory.WeaponDataArray[_inventory.WeaponArrayIndex]);
    }

    void UseConsumibleObject()
    {
        if (Input.GetButtonDown("Consume1")  && _inventory.ConsumibleItemArray[0] != null)
        {   
            (_inventory.ConsumibleItemArray[0] as ConsumibleSO).Consume(0);
            UIManager.Instance.UpdateConsumibleInventory();
            UIManager.Instance.UpdateCurrentLife();
        }

        if (Input.GetButtonDown("Consume2")  && _inventory.ConsumibleItemArray[1] != null)
        {
            (_inventory.ConsumibleItemArray[1] as ConsumibleSO).Consume(1);
            UIManager.Instance.UpdateConsumibleInventory();
            UIManager.Instance.UpdateCurrentLife();
        }
    }
}
