using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookToMoseDirection : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        LookAtMouse();
    }

    private void FixedUpdate()
    {
        
    }
    
    void LookAtMouse()
    {
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float _angle = Vector2.SignedAngle(Vector2.right, direction);
        transform.eulerAngles = new Vector3(0, 0, _angle - 90);

        /*
        transform.LookAt(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector3.forward);
        transform.eulerAngles = new Vector3(0, 0, -transform.eulerAngles.z);
        */
    }

}
