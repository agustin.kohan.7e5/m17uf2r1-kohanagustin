using System.Collections;
using UnityEngine;


public class Player : Character, IDamageable, IKnockeable
{
    public PlayerDataSO _playerData;
    DashController _dashController;

    protected override void Awake()
    {
        if (GameObject.Find("Player").gameObject != this.gameObject)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);
        SetSOValues();
        _playerData.inventory.ClearInventory();
        _dashController = GetComponent<DashController>();
        base.Awake();
    }

    void Update()
    {
        if (_dashController.Dash(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"))))
        {
            Intangible(_playerData.DashDuration);
            Stun(0.5f);
        }
    }

    private bool IsIntangible = false;
    public void Intangible(float IntangibilityTime) => StartCoroutine(BecameIntangibleForSeconds(IntangibilityTime));
    IEnumerator BecameIntangibleForSeconds(float IntangibilityTime)
    {
        IsIntangible = true;
        yield return new WaitForSecondsRealtime(IntangibilityTime);
        IsIntangible = false;
    }

    public void TakeDamage(float damageTaken)
    {
        if (!IsIntangible)
        {
            _playerData.CurrentHealth -= damageTaken;
            UIManager.Instance.UpdateCurrentLife();
            if (_playerData.CurrentHealth <= 0)
                Die();
        }
    }

    public virtual void ReciveHeal(float healthRecived)
    {
        if (_playerData.CurrentHealth + healthRecived > _playerData._maxHealth)
            _playerData.CurrentHealth = _playerData._maxHealth;
        else
            _playerData.CurrentHealth += healthRecived;

        UIManager.Instance.UpdateCurrentLife();
    }
    public override void Die()
    {
        GameManager.Instance.GameOver();
    }
    
    public void ReciveKnockBack(Vector2 OriginPoint, float KnockbackForce)
    {
        Stun(0.2f);
        _rb.AddForce(-OriginPoint * KnockbackForce);
    }

    void SetSOValues()
    {
        _playerData = (PlayerDataSO)ScriptableObject.CreateInstance(typeof(PlayerDataSO));
        _playerData.SetValues(_characterData as PlayerDataSO);
        _playerData.CurrentHealth = _playerData._maxHealth;
    }
}
