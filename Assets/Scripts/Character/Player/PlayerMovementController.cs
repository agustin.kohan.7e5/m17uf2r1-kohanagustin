using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    Rigidbody2D _rb;
    Vector2 _getAxis;
    Player _player;
    // Start is called before the first frame update
    void Awake()
    {
        _player = GetComponent<Player>();
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(_player._moveState == moveState.Active)
            _getAxis = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    private void FixedUpdate()
    {
        if(_player._moveState == moveState.Active)
            Move();
    }

    private void Move()
    {
        _rb.velocity = new Vector2(_getAxis.x * _player._playerData.CurrentSpeed * Time.deltaTime * 100, _getAxis.y * _player._playerData.CurrentSpeed * Time.deltaTime * 100);
    }
}
