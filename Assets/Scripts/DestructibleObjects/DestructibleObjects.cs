using UnityEngine;

public class DestructibleObjects : MonoBehaviour, IDamageable
{
    [SerializeField]
    ItemListSO _itemList;

    public void DestroyTheObject()
    {
        Instantiate(ChoseRandomItem(), new Vector2(transform.position.x, transform.position.y), transform.rotation);
        Destroy(this.gameObject);
    }

    public void TakeDamage(float damageTaken)
    {
        DestroyTheObject();
    }

    private GameObject ChoseRandomItem() => _itemList.itemArray[Random.Range(0, _itemList.itemArray.Length)];
}
