using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    GameObject _player;

    private void Start()
    {
        _player = GameManager.Instance.Player.gameObject;
    }
    // Update is called once per frame
    void Update()
    {
        FollowPlayer();
    }

    private void FollowPlayer()
    {
        if (_player != null)
            transform.position = new Vector3(_player.transform.position.x, _player.transform.position.y, transform.position.z);
    }
}
