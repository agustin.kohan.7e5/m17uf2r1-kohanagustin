using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverController : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI _lastScoreText;
    [SerializeField]
    TextMeshProUGUI _bestScoreText;
    [SerializeField]
    TextMeshProUGUI _bestPlayerText;

    [SerializeField]
    GameObject _highScoreText;
    CharacterData characterdata;

    private void Awake()
    {
        characterdata = GameObject.Find("CharacterData").GetComponent<CharacterData>();
        _lastScoreText.text = characterdata.playerScore.ToString();
        _bestScoreText.text = PlayerPrefs.GetInt("BestScore").ToString();
        _bestPlayerText.text = PlayerPrefs.GetString("BestScoreName");

        CheckToThrowTheNewHighScoreEvent();
    }

    private void CheckToThrowTheNewHighScoreEvent()
    {
        if (PlayerPrefs.GetInt("BestScore") != null && PlayerPrefs.GetInt("BestScore") < characterdata.playerScore)
        {
            PlayerPrefs.SetInt("BestScore", characterdata.playerScore);
            PlayerPrefs.SetString("BestScoreName", characterdata.playerName);

            _bestScoreText.text = PlayerPrefs.GetInt("BestScore").ToString();
            _bestPlayerText.text = PlayerPrefs.GetString("BestScoreName");
            _highScoreText.SetActive(true);
        }
    }

    public void ReturnToMenu()
    {
        Destroy(characterdata.gameObject);
        SceneManager.LoadScene("TitleScene");
    }
}
