using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterData : MonoBehaviour
{
    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }
    public string playerName;
    public int playerScore;

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (GameObject.Find("Player"))
            UIManager.Instance._playerNameText.text = GameObject.Find("Player").GetComponent<Player>()._playerData.Name = playerName;
        
    }
}
