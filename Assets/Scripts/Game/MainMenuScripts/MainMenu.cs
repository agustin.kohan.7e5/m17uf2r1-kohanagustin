using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    TMP_InputField nameField;
    [SerializeField]
    CharacterData PlayerName;

    public void SetPlayerName() => PlayerName.playerName = nameField.text;

    public void StartGame()
    {
        if (PlayerName.playerName != "")
            SceneManager.LoadScene("GameplayScene");
    }

    public void ExitGame() => Application.Quit();
}
