using UnityEngine;
using UnityEngine.SceneManagement;


public enum CurrentLevel
{
    Level1,
    Level2,
    Level3
};

public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;
    public static GameManager Instance { get { return instance; } }

    public Player Player { get; private set; }
    public int CurrentAmountOfEnemyInScene;

    [SerializeField]
    private static CurrentLevel _actualLevel = CurrentLevel.Level1;
    public static CurrentLevel ActualLevel
    {
        get => _actualLevel;
        set => _actualLevel = value;
    }

    [HideInInspector]
    public int ShoopCounter;

    void Awake()
    {
        SingletonInstance();
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        Player = GameObject.Find("Player").GetComponent<Player>();
    }

    void SingletonInstance()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this) { Destroy(this.gameObject); }
        instance = this;
    }
    
    public void SetPlayerPosition(Vector3 playerPosition) => Player.gameObject.transform.position = playerPosition;
    public Vector3 GetPlayerPosition() => Player.gameObject.transform.position;
    public void AddCoinsToPlayer(int coinsToAdd) => Player._playerData.CurrentCoins += coinsToAdd;
    public void AddScoreToPlayer(int scoreToAdd) => Player._playerData.CurrentScore += scoreToAdd;

    
    // Update is called once per frame
    private void Update()
    {
        OpenPanel();
    }

    public void GameOver()
    {
        GameObject.Find("CharacterData").GetComponent<CharacterData>().playerScore = (int)(Player._playerData.CurrentScore + Player._playerData.CurrentCoins);
        SceneManager.LoadScene("GameOverScene");
    }

    private void OpenPanel()
    {
        if (Input.GetButtonDown("OpenShop") && ShoopCounter == 5)
        {
            UIManager.Instance.OpenShopPanel().OpenShoop();
            ShoopCounter = 0;
            UIManager.Instance.UpdateShoopBar();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            UIManager.Instance.OpenSettingsPanel().OnPanelOpen();
        

    }
    public void NextLevel()
    {
        switch (ActualLevel)
        {
            case CurrentLevel.Level1:
                _actualLevel = CurrentLevel.Level2;
                break;
            case CurrentLevel.Level2:
                _actualLevel = CurrentLevel.Level3;
                break;
            case CurrentLevel.Level3:
                GameOver();
                break;
            default:
                break;
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ResetScene() => SceneManager.LoadScene("GameplayScene");
}
