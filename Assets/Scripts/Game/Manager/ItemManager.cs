using UnityEngine;

public class ItemManager : MonoBehaviour
{
    private static ItemManager instance = null;
    public static ItemManager Instance { get { return instance; } }

    void SingletonInstance()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this) { Destroy(this.gameObject); }

        instance = this;
    }

    private void Awake()
    {
        SingletonInstance();
    }


    [SerializeField]
    ItemDataSO[] itemDataArray;

    public ItemDataSO GetRandomItem() => itemDataArray[Random.Range(0, itemDataArray.Length - 1)];
}
