using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    private static UIManager instance = null;
    public static UIManager Instance { get { return instance; } }

    GameObject _canvas;
    GameObject _shopPanel;
    GameObject _settingsPanel;

    PlayerDataSO _playerData;

    [SerializeField]
    GameObject PlayerStats;
    Slider _lifeBar;
    TextMeshProUGUI _lifeText;
    [HideInInspector]
    public TextMeshProUGUI _playerNameText;
    TextMeshProUGUI _coinsText;
    TextMeshProUGUI _scoreText;

    [SerializeField]
    GameObject PlayerInventory;
    Image _weaponInventoryImage;
    TextMeshProUGUI _ammoText;
    Image _consumible1Image;
    Image _consumible2Image;

    [SerializeField]
    Slider _shopBar;

    [SerializeField]
    Sprite _defaultSprite;

    private void Awake()
    {
        SingletonInstance();
        _canvas = GameObject.Find("Canvas");
        _shopPanel = _canvas.transform.Find("ShoopPanel").gameObject;
        _playerData = GameObject.Find("Player").GetComponent<Player>()._playerData;

        _lifeBar = PlayerStats.transform.Find("LifeBar").GetComponent<Slider>();
        _lifeText = _lifeBar.transform.Find("LifeText").GetComponent<TextMeshProUGUI>();
            UpdateCurrentLife();
        _playerNameText = PlayerStats.transform.Find("PlayerNameText").GetComponent<TextMeshProUGUI>();
        _playerNameText.text = _playerData.Name;
        _coinsText = PlayerStats.transform.Find("CoinsText").GetComponent<TextMeshProUGUI>();
            UpdateCurrentCoins();
        _scoreText = PlayerStats.transform.Find("ScoreText").GetComponent<TextMeshProUGUI>();
            UpdateScoreText();

        _weaponInventoryImage = PlayerInventory.transform.Find("Weapons").transform.Find("WeaponImage").GetComponent<Image>();
        _ammoText = PlayerInventory.transform.Find("Weapons").transform.Find("AmmoText").GetComponent<TextMeshProUGUI>();
        UpdateWeaponInventory();
        
        _consumible1Image = PlayerInventory.transform.Find("Consumibles").transform.Find("Consumible1").GetComponent<Image>();
        _consumible2Image = PlayerInventory.transform.Find("Consumibles").transform.Find("Consumible2").GetComponent<Image>();
        UpdateConsumibleInventory();

        _settingsPanel = _canvas.transform.Find("SettingsPanel").gameObject;
    }

    void SingletonInstance()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this) { Destroy(this.gameObject); }

        instance = this;
    }

    internal ShoopController OpenShopPanel() {
        _shopPanel.SetActive(true);
        return _shopPanel.GetComponent<ShoopController>();
    }

    internal SettingsController OpenSettingsPanel()
    {
        _settingsPanel.SetActive(true);
        return _settingsPanel.GetComponent<SettingsController>();
    }

    public void UpdateCurrentLife()
    {
        _lifeBar.value = _playerData.CurrentHealth / _playerData._maxHealth;
        _lifeText.text = _playerData.CurrentHealth + "/" + _playerData._maxHealth;
    }

    public void UpdateCurrentCoins() => _coinsText.text = _playerData.CurrentCoins.ToString();

    public void UpdateScoreText() => _scoreText.text = _playerData.CurrentScore.ToString();

    public void UpdateShoopBar() => _shopBar.value = GameManager.Instance.ShoopCounter;

    public void UpdateWeaponInventory()
    {
        if (_weaponInventoryImage.sprite != _playerData.inventory.WeaponDataArray[_playerData.inventory.WeaponArrayIndex].ItemSprite)
            _weaponInventoryImage.sprite = _playerData.inventory.WeaponDataArray[_playerData.inventory.WeaponArrayIndex].ItemSprite;
        _ammoText.text = _playerData.inventory.WeaponDataArray[_playerData.inventory.WeaponArrayIndex].CurrentAmmo + "/" + _playerData.inventory.WeaponDataArray[_playerData.inventory.WeaponArrayIndex].MaxAmmo * _playerData.inventory.WeaponDataArray[_playerData.inventory.WeaponArrayIndex].CurrentLoaders;
    }

    public void UpdateConsumibleInventory()
    {
         if (_playerData.inventory.ConsumibleItemArray[0] != null)
            _consumible1Image.sprite = _playerData.inventory.ConsumibleItemArray[0].ItemSprite;
         else
            _consumible1Image.sprite = _defaultSprite;


        if (_playerData.inventory.ConsumibleItemArray[1] != null)
            _consumible2Image.sprite = _playerData.inventory.ConsumibleItemArray[1].ItemSprite;
        else
            _consumible2Image.sprite = _defaultSprite;
    }
}
