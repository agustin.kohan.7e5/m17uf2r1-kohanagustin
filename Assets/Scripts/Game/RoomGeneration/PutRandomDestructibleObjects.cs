using System.Globalization;
using UnityEngine;

public class PutRandomDestructibleObjects : MonoBehaviour
{

    [SerializeField]
    GameObject[] _destructibleObjects;

    public int NumberOfObjetsToPut;
    new BoxCollider2D collider2D;

    // Start is called before the first frame update
    void Awake()
    {
        collider2D = transform.Find("EnterDetector").GetComponent<BoxCollider2D>();
        for (int i = 0; i < NumberOfObjetsToPut; i++)
        {
            Instantiate(GetRandomObject(), GetRandomPositon(), Quaternion.identity);
        }
    }

    private GameObject GetRandomObject() => _destructibleObjects[Random.Range(0, _destructibleObjects.Length)].gameObject;
    private Vector2 GetRandomPositon() => new Vector2(Random.Range(collider2D.bounds.min.x, collider2D.bounds.max.x), Random.Range(collider2D.bounds.min.y, collider2D.bounds.max.y));
    
}
