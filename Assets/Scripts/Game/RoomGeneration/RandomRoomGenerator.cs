using UnityEngine;

public class RandomRoomGenerator : MonoBehaviour
{
    public static RandomRoomGenerator Instance;
    public int x, y;
    private int[,] gameMatrix;
    private int[,] matrixRooms;
    public GameObject FirstRoom;
    public GameObject[] RandomRooms;
    private GameObject _randomRoom;
    public GameObject FinalRoom;
    [HideInInspector] public int finnishPoint;
    [HideInInspector] public int startPoint;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }

        CreateWorld();
    }

    void CreateWorld()
    {
        switch (GameManager.ActualLevel)
        {
            case CurrentLevel.Level1:
                x = 5;  y = 5;
                break;

            case CurrentLevel.Level2:
                x = 10; y = 10;
                break;

            case CurrentLevel.Level3:
                x = 20; y = 20;
                break;
        }
        gameMatrix = new int[x, y];
        matrixRooms = new int[x, y];
        MakeRoute();
        DeterminateBlocks();
        CreateBlocks();
    }

    void MakeRoute()
    {
        startPoint = Random.Range(0, x);
        finnishPoint = Random.Range(0, x);


        int[] tracer = { startPoint, y - 1 };
        while (!(tracer[0] == finnishPoint && tracer[1] == 0))
        {
            int movement;
            bool _repeat = false;
            do
            {
                _repeat = false;

                movement = Random.Range(1, 4);
                switch (movement)
                {
                    case 1:
                        tracer[0]++;
                        if (tracer[0] >= x)
                        {
                            tracer[0]--;
                            _repeat = true;
                        }
                        break;
                    case 2:
                        tracer[1]--;
                        if (tracer[1] < 0)
                        {
                            tracer[1]++;
                            _repeat = true;
                        }
                        break;
                    case 3:
                        tracer[0]--;
                        if (tracer[0] < 0)
                        {
                            tracer[0]++;
                            _repeat = true;
                        }
                        break;
                    default:
                        break;
                }
            } while (_repeat);

            gameMatrix[tracer[0], tracer[1]] = 3;
        }
        gameMatrix[startPoint, y - 1] = 1;
        gameMatrix[finnishPoint, 0] = 2;


        GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(startPoint * FirstRoom.GetComponent<BoxCollider2D>().size.x, (y - 1) * FirstRoom.GetComponent<BoxCollider2D>().size.y, 0);
    }

    void DeterminateBlocks()
    {

        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                int result = 0;
                result += 1 * CheckIfThereIsABlock(i, j + 1);
                result += 2 * CheckIfThereIsABlock(i + 1, j);
                result += 4 * CheckIfThereIsABlock(i, j - 1);
                result += 8 * CheckIfThereIsABlock(i - 1, j);
                matrixRooms[i, j] = result;
            }
        }

    }

    public GameObject GetRandomRoom() => RandomRooms[Random.Range(0, RandomRooms.Length)];

    private void CreateBlocks()
    {
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                if (gameMatrix[i, j] != 0)
                {
                    GameObject Room;

                    if (gameMatrix[startPoint, y - 1] == gameMatrix[i, j])
                    {
                        Room = Instantiate(FirstRoom, new Vector3(i * FirstRoom.GetComponent<BoxCollider2D>().size.x, j * FirstRoom.GetComponent<BoxCollider2D>().size.y, 0), Quaternion.identity);
                        Room.name = "FirstRoom";
                        Room.tag = "FirstRoom";
                    }
                    else if (gameMatrix[finnishPoint, 0] == gameMatrix[i, j])
                    {
                        Room = Instantiate(FinalRoom, new Vector3(i * FinalRoom.GetComponent<BoxCollider2D>().size.x, j * FinalRoom.GetComponent<BoxCollider2D>().size.x, 0), Quaternion.identity);
                        Room.name = "FinalRoom";
                        Room.tag = "FinalRoom";
                    }

                    else
                    {
                        _randomRoom = GetRandomRoom();
                        Room = Instantiate(_randomRoom, new Vector3(i * _randomRoom.GetComponent<BoxCollider2D>().size.x, j * _randomRoom.GetComponent<BoxCollider2D>().size.y, 0), Quaternion.identity);
                        Room.name = "Room";
                    }

                    Room.GetComponent<Rooms>().Initializer(matrixRooms[i, j]);
                    Room.transform.SetParent(this.transform);
                }

            }
        }
    }

    private int CheckIfThereIsABlock(int _x, int _y)
    {
        if (_x < 0 || _y < 0 || _x >= x || _y >= y)
        {
            return 0;
        }
        if (gameMatrix[_x, _y] == 0)
        {
            return 0;
        }
        return 1;
    }
}
