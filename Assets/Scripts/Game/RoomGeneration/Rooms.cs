using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Rooms : MonoBehaviour
{
    private int _door;
    public GameObject[] Doors;

    private RoomsEvent _roomsEvent;
    private List<GameObject> _functionalDoors = new List<GameObject>();


    private void Awake()
    {   
        _roomsEvent = transform.Find("EnterDetector")?.GetComponent<RoomsEvent>();
        _roomsEvent.OnEnter += CloseDoors;
    }

    private void CloseDoors(object sender, EventArgs e)
    {
        foreach (var Door in _functionalDoors)
            Door.SetActive(true);

        _roomsEvent.OnAllEnemiesDefeated += OpenDoors;
        _roomsEvent.OnEnter -= CloseDoors;
    }

    private void OpenDoors(object sender, EventArgs e)
    {
        foreach (var Door in _functionalDoors)
            Door.SetActive(false);

        GameManager.Instance.Player._playerData.CurrentScore += 50;
        UIManager.Instance.UpdateScoreText();
        _roomsEvent.OnAllEnemiesDefeated -= OpenDoors;
    }

    public void Initializer(int positionOfDoor)
    {
        _door = positionOfDoor;
        
        if ((_door % 2) != 1 == false)
        {
            Doors[0].GetComponent<SpriteRenderer>().color = Color.black;
            _functionalDoors.Add(Doors[0]);
            Doors[0].SetActive(false);
        }
        if ((Mathf.FloorToInt(_door / 2) % 2) != 1 == false)
        {
            Doors[1].GetComponent<SpriteRenderer>().color = Color.black;
            _functionalDoors.Add(Doors[1]);
            Doors[1].SetActive(false);
        }
        if ((Mathf.FloorToInt(_door / 8) % 2) != 1 == false)
        {
            Doors[2].GetComponent<SpriteRenderer>().color = Color.black;
            _functionalDoors.Add(Doors[2]);
            Doors[2].SetActive(false);
        }
        if ((Mathf.FloorToInt(_door / 4) % 2) != 1 == false)
        {
            Doors[3].GetComponent<SpriteRenderer>().color = Color.black;
            _functionalDoors.Add(Doors[3]);
            Doors[3].SetActive(false);
        }
    }
}
