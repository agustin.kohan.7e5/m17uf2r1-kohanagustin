using System;
using UnityEngine;

public class RoomsEvent : MonoBehaviour
{
    public event EventHandler OnEnter;
    public event EventHandler OnAllEnemiesDefeated;
    private void OnTriggerEnter2D(Collider2D collision)
    {      
        if(collision.CompareTag("Player"))
            OnEnter?.Invoke(this, EventArgs.Empty);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnEnter?.Invoke(this, EventArgs.Empty);
        }

        if (GameManager.Instance.CurrentAmountOfEnemyInScene == 0)
        {
            RoomFinshed();
        }
    }
    private void RoomFinshed()
    {
        OnAllEnemiesDefeated?.Invoke(this, EventArgs.Empty);
    }

}
