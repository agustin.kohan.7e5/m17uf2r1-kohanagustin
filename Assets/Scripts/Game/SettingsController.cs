using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsController : MonoBehaviour
{
    [SerializeField] AudioMixer audioMixer;

    [SerializeField]
    Slider _volumeMusicSlider;
    [SerializeField]
    TextMeshProUGUI _volumeMusicPercentageText;

    [SerializeField]
    Slider _volumeSFXSlider;
    [SerializeField]
    TextMeshProUGUI _volumeSFXPercentageText;

    const string Mixer_Music = "MusicVolume";
    const string Mixer_SFX = "SFXVolume";

    public void Awake()
    {
        _volumeMusicSlider.onValueChanged.AddListener(ChangeMusicVolumeValues);
        _volumeSFXSlider.onValueChanged.AddListener(ChangeSFXVolumeValues);

    }
    public void OnPanelOpen()
    {
        Time.timeScale = 0;
    }

    public void ClosePanel()
    {
        Time.timeScale = 1;
        this.gameObject.SetActive(false);
    }

    public void ChangeMusicVolumeValues(float value)
    {
        audioMixer.SetFloat(Mixer_Music, Mathf.Log10(value) * 20);
        _volumeMusicPercentageText.text = (int)(_volumeMusicSlider.value * 100) + "%"; 
    }

    public void ChangeSFXVolumeValues(float value)
    {
        audioMixer.SetFloat(Mixer_SFX, Mathf.Log10(value) * 20);
        _volumeSFXPercentageText.text = (int)(_volumeSFXSlider.value * 100) + "%";
    }

}
