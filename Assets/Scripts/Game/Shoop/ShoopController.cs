using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShoopController : MonoBehaviour
{
    PlayerDataSO playerData;
    ItemDataSO[] purcheableItem = new ItemDataSO[3];

    [SerializeField]
    Button[] _itemButton;

    [SerializeField]
    Sprite _defaultSprite;
    
    internal void Awake()
    {
        playerData = GameManager.Instance.Player._playerData;   
    }

    public void OpenShoop()
    {
        Time.timeScale = 0;
        GenerateRandomItems();
        SetItemDataOnShoop();
    }

    public void GenerateRandomItems()
    {
        for (int i = 0; i < purcheableItem.Length; i++)
            purcheableItem[i] = ItemManager.Instance.GetRandomItem();
        
    }

    private void SetItemDataOnShoop()
    {
        for (int i = 0; i < _itemButton.Length; i++)
        {
            if (purcheableItem[i] != null) {
                _itemButton[i].image.sprite = purcheableItem[i].ItemSprite;
                _itemButton[i].GetComponentInChildren<TextMeshProUGUI>().text = purcheableItem[i].Name + "\n" + purcheableItem[i].Price.ToString();
            }
            else
            {
                _itemButton[i].image.sprite = _defaultSprite;
                _itemButton[i].GetComponentInChildren<TextMeshProUGUI>().text = "Sin existencias";
            }
        }
    }

    public void BuyItem(int shoopSlot)
    {
        if (purcheableItem[shoopSlot] != null && playerData.CurrentCoins >= purcheableItem[shoopSlot].Price)
        {
            if (purcheableItem[shoopSlot] is WeaponDataSO)
                for (int i = 0; i < playerData.inventory.WeaponDataArray.Length; i++)
                {
                    if (playerData.inventory.WeaponDataArray[i] == null)
                    {
                        playerData.CurrentCoins -= purcheableItem[shoopSlot].Price;
                        playerData.inventory.WeaponDataArray[i] = Instantiate(purcheableItem[shoopSlot] as WeaponDataSO); ;
                        purcheableItem[shoopSlot] = null; // Add default item no purchaseable
                        break;
                    }
            }

            if (purcheableItem[shoopSlot] is ConsumibleSO)
                for (int i = 0; i < playerData.inventory.ConsumibleItemArray.Length; i++)
            {
                if (playerData.inventory.ConsumibleItemArray[i] == null)
                {
                    playerData.CurrentCoins -= purcheableItem[shoopSlot].Price;
                    playerData.inventory.ConsumibleItemArray[i] = purcheableItem[shoopSlot];
                    
                    purcheableItem[shoopSlot] = null; // Add default iten no purchaseable
                    UIManager.Instance.UpdateConsumibleInventory();
                    break;
                }
            }
            UIManager.Instance.UpdateCurrentCoins();
            SetItemDataOnShoop();
        }
    }
    public void CloseShop()
    {
        Time.timeScale = 1;
        this.gameObject.SetActive(false);
    }
}
