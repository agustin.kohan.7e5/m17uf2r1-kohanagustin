public interface IConsumible
{
    public void Consume(PlayerDataSO playerData);
}
