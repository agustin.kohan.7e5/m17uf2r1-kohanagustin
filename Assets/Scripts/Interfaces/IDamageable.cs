public interface IDamageable
{
    public void TakeDamage(float damageTaken);
}
