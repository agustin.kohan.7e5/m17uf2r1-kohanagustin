using UnityEngine;

public interface IKnockeable
{
    public void ReciveKnockBack(Vector2 OriginPoint, float KnockbackForce);
}
