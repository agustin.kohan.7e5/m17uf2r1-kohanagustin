using UnityEngine;

public interface ITakeable
{
    public void OnTake(GameObject player);
}
