using UnityEngine;

public class ConsumibleItem : Item, ITakeable
{
    public void OnTake(GameObject player)
    {
        var consumibleInventory = (player.GetComponent<Player>()._playerData.inventory.ConsumibleItemArray);

        for (int i = 0; i < consumibleInventory.Length; i++)
        {
            if (consumibleInventory[i] == null)
            {
                consumibleInventory[i] = _itemDataSO;
                UIManager.Instance.UpdateConsumibleInventory();
                Destroy(this.gameObject);
                break;
            }
        }
    }
}
