using UnityEngine;

public class WeaponItem : Item, ITakeable
{
    public void OnTake(GameObject player)
    {
        var weaponInventory = player.GetComponent<Player>()._playerData.inventory.WeaponDataArray;

        for (int i = 0; i < weaponInventory.Length; i++)
        {
            if (weaponInventory[i] == null) 
            {
                weaponInventory[i] = Instantiate(_itemDataSO as WeaponDataSO);
                Destroy(this.gameObject);
                break;
            }
        }
    }
}
