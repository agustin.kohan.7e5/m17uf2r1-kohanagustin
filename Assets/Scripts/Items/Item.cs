using UnityEngine;

public abstract class Item : MonoBehaviour
{
    [SerializeField]
    protected ItemDataSO _itemDataSO;

    private void Awake()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = _itemDataSO.ItemSprite;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.TryGetComponent<ITakeable>(out ITakeable takeable) && collision.gameObject.CompareTag("Player"))
            takeable.OnTake(collision.gameObject);
    }
}
