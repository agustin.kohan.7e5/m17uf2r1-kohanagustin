using Unity.VisualScripting;
using UnityEngine;

public class BouncyBullet : Bullet
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.TryGetComponent<IDamageable>(out IDamageable damageable))
        {
            damageable.TakeDamage(Damage);
            Destroy(this.gameObject);
        }
    }
}
