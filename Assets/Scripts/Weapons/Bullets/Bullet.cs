using Unity.VisualScripting;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float Damage;
    public float BulletSpeed;
    [SerializeField]
    private float _timeLife;
    private float _timer = 0;

    protected virtual void Awake()
    { 
        float z = transform.eulerAngles.z + 90;
        GetComponent<Rigidbody2D>().velocity = BulletSpeed * new Vector2(Mathf.Cos(z * Mathf.Deg2Rad) , Mathf.Sin(z * Mathf.Deg2Rad));
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;
        if (_timer >= _timeLife) Destroy(this.gameObject);
    }

    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent<IDamageable>(out IDamageable damageable))
        {
            damageable.TakeDamage(Damage);
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.CompareTag("Wall"))
            Destroy(gameObject);
    }
}
