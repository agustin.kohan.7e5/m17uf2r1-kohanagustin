using UnityEngine;

public class StunBullet : Bullet
{
    public float StunTime;

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Character>(out Character character))
        {
            character.Stun(StunTime);
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.CompareTag("Wall"))
            Destroy(gameObject);
    } 
}
