using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssaultRifle : Weapon
{
    public override void Shoot()
    {
        base.Shoot();
    }

    public override void SetValues(WeaponContext weaponContext) => _weaponData = weaponContext.WeaponsList.AssaultRifle;
}
