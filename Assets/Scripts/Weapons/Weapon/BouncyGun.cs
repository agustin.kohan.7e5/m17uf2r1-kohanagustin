public class BouncyGun : Weapon
{
    public override void Shoot()
    {
        base.Shoot();
    }

    public override void SetValues(WeaponContext weaponContext) => _weaponData = weaponContext.WeaponsList.BouncyGun;
}
