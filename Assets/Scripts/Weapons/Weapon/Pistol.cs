public class Pistol : Weapon
{

    private void Awake()
    {
    }
    public override void Shoot()
    {
        base.Shoot();
    }

    public override void SetValues(WeaponContext weaponContext) => _weaponData = weaponContext.WeaponsList.Pistol;
}
