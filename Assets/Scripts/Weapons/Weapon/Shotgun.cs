using UnityEngine;

public class Shotgun : Weapon
{
    public override void Shoot()
    {
        if (Input.GetButton("Fire1") && _weaponState == WeaponState.Active && _weaponData.CurrentAmmo > 0)
        {
            Debug.Log("Shoot");
            for (int i = 0; i < _weaponData.BulletXShot; i++)
            {
                _bullet = Instantiate(_weaponData.Bullet, transform.position, new Quaternion(transform.parent.rotation.x, transform.parent.rotation.y, transform.parent.rotation.z + Random.Range(-_weaponData.Dispersion, _weaponData.Dispersion), transform.parent.rotation.w));
                _bullet.GetComponent<Bullet>().Damage = _weaponData.Damage;
            }
            _weaponData.CurrentAmmo -= _weaponData.BulletXShot;
            StartCoroutine(Shooting(_weaponData.Cadence));
        }
    }
    
    public override void SetValues(WeaponContext weaponContext) => _weaponData = weaponContext.WeaponsList.Shotgun;
}
