using System.Collections;
using UnityEngine;

public enum WeaponState
{
    Active,
    Shooting,
    Reloading
}

public class Weapon : MonoBehaviour
{
    private AudioSource audioSource;
    protected WeaponState _weaponState = WeaponState.Active;
    protected GameObject _bullet;
    public WeaponDataSO _weaponData;

    private void Awake()
    {
        audioSource= GetComponent<AudioSource>();
    }

    private void Update()
    {
        Reload();
        Shoot();   
    }
    
    public void ChangeWeaponValues(WeaponDataSO newWeaponData)
    {
        _weaponData = newWeaponData;
        UIManager.Instance.UpdateWeaponInventory();
    }

    public virtual void Shoot()
    {
        if (Input.GetButton("Fire1") && _weaponState == WeaponState.Active && _weaponData.CurrentAmmo > 0)
        {
            audioSource.Play();
            for (int i = 0; i < _weaponData.BulletXShot; i++)
            {
                _bullet = Instantiate(_weaponData.Bullet, transform.position, new Quaternion(transform.parent.rotation.x, transform.parent.rotation.y, transform.parent.rotation.z + Random.Range(-_weaponData.Dispersion, _weaponData.Dispersion), transform.parent.rotation.w));
                _bullet.GetComponent<Bullet>().Damage = _weaponData.Damage;
            }
            _weaponData.CurrentAmmo -= _weaponData.BulletXShot;
            UIManager.Instance.UpdateWeaponInventory();
            StartCoroutine(Shooting(_weaponData.Cadence));
        }
    }

    protected IEnumerator Shooting(float cadenceShoot)
    {
        _weaponState = WeaponState.Shooting;
        yield return new WaitForSecondsRealtime(cadenceShoot);
        _weaponState = WeaponState.Active;
    }

    public virtual void Reload()
    {
        if (Input.GetButtonDown("Reload") && _weaponState == WeaponState.Active && 
            _weaponData.CurrentAmmo < _weaponData.MaxAmmo && _weaponData.CurrentLoaders > 0)
        {
            _weaponState = WeaponState.Reloading;
            StartCoroutine(Reloading(_weaponData.ReloadTime));
        }
    }

    IEnumerator Reloading(float reloadTime)
    {
        _weaponState = WeaponState.Reloading;
        yield return new WaitForSecondsRealtime(reloadTime);
        _weaponData.CurrentAmmo = _weaponData.MaxAmmo;
        _weaponData.CurrentLoaders--;
        _weaponState = WeaponState.Active;
        UIManager.Instance.UpdateWeaponInventory();
    }

    public virtual void SetValues(WeaponContext weaponContext) { }
}
