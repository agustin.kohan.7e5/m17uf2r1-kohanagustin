using System;
using UnityEngine;

public class WeaponContext : MonoBehaviour
{
    public WeaponListSO WeaponsList;

    protected enum WeaponType
    {
        Pistol,
        Shotgun,
        AssaultRifle,
        Sniper,
        BouncyGun
    }
    Weapon _weapon;

    WeaponType _weaponType;
    private int _indexWeponList;
    
    // Start is called before the first frame update
    void Start()
    {
        _weapon = gameObject.AddComponent<Pistol>();
        _indexWeponList = 0;
        _weapon.SetValues(this);
        SetDefaultValuesToDefaultWeapon();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(_weaponType);
        if (Input.GetButtonDown("ScrollUpInList") || Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (_indexWeponList < Enum.GetValues(typeof(WeaponType)).Length)
                ChangeWeapon(1);
        }
            
        if (Input.GetButtonDown("ScrollDownInList") || Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (_indexWeponList > 0)
                ChangeWeapon(-1);            
        }
    }

    private void ChangeWeapon(int i)
    {
        _indexWeponList += i;
        ChangeWeaponType();
        ChargeCurrentWeaponValues();
    }

    private void ChangeWeaponType()
    {
        switch (_indexWeponList)
        {
            case 0:
                _weaponType = WeaponType.Pistol;
                break;
            case 1:
                _weaponType = WeaponType.Shotgun;
                break;
            case 2:
                _weaponType = WeaponType.AssaultRifle;
                break;
            case 3:
                _weaponType = WeaponType.Sniper;
                break;
            case 4:
                _weaponType = WeaponType.BouncyGun;
                break;
            default:
                break;
        }
    }

    private void ChargeCurrentWeaponValues()
    {
        Destroy(_weapon);
        switch (_weaponType)
        {
            case WeaponType.Pistol:
                _weapon = gameObject.AddComponent<Pistol>();
                break;
            case WeaponType.Shotgun:
                _weapon = gameObject.AddComponent<Shotgun>();
                break;
            case WeaponType.AssaultRifle:
                _weapon = gameObject.AddComponent<AssaultRifle>(); 
                break;
            case WeaponType.Sniper:
                _weapon = gameObject.AddComponent<Sniper>();
                break;
            case WeaponType.BouncyGun:
                _weapon = gameObject.AddComponent<BouncyGun>();
                break;
            default:
                break;
        }

        _weapon.SetValues(this);
    }

    private void SetDefaultValuesToDefaultWeapon()
    {
        _weapon._weaponData.CurrentAmmo = _weapon._weaponData.MaxAmmo;
        _weapon._weaponData.CurrentLoaders = 10;
    }
}
